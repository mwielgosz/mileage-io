package com.infiniwaresolutions.mileageio.database

import android.provider.BaseColumns

object MaintenanceContract {

    /* Inner class that defines the table contents */
    class MaintenanceEntry : BaseColumns {
        companion object {
            const val TABLE_NAME = "maintenance"
            const val COLUMN_MAINTENANCE_ID = "id"
            const val COLUMN_VEHICLE_ID = "vehicleId"
            const val COLUMN_TYPE = "type"
            const val COLUMN_DATE = "date"
            const val COLUMN_PREV_MILEAGE = "previousMileage"
            const val COLUMN_MILEAGE = "mileage"
            //const val COLUMN_CURRENT_MPG = "currentMPG"
            const val COLUMN_FUEL_ADDED = "fuelAdded"
        }
    }
}