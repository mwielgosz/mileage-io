package com.infiniwaresolutions.mileageio.database

import android.provider.BaseColumns

object VehicleContract {
    /* Inner class that defines the table contents */
    class VehicleEntry : BaseColumns {
        companion object {
            const val TABLE_NAME = "vehicles"
            const val COLUMN_VEHICLE_ID = "id"
            const val COLUMN_YEAR = "year"
            const val COLUMN_MAKE = "make"
            const val COLUMN_MODEL = "model"
            const val COLUMN_MILEAGE = "mileage"
            const val COLUMN_AVERAGE_MPG = "averageMPG"
        }
    }

}