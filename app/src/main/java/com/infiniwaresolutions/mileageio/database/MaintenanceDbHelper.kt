package com.infiniwaresolutions.mileageio.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.infiniwaresolutions.mileageio.models.Maintenance
import java.util.*


class MaintenanceDbHelper(context: Context?) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    companion object {
        // If you change the database schema, you must increment the database version.
        private const val TAG = "MaintenanceDbHelper"
        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "MaintenanceDatabase.db"

        private const val SQL_CREATE_ENTRIES =
                "CREATE TABLE IF NOT EXISTS " + MaintenanceContract.MaintenanceEntry.TABLE_NAME + " (" +
                        MaintenanceContract.MaintenanceEntry.COLUMN_MAINTENANCE_ID + " INTEGER PRIMARY KEY," +
                        MaintenanceContract.MaintenanceEntry.COLUMN_VEHICLE_ID + " INTEGER, " +
                        MaintenanceContract.MaintenanceEntry.COLUMN_TYPE + " TEXT, " +
                        MaintenanceContract.MaintenanceEntry.COLUMN_DATE + " TEXT, " +
                        MaintenanceContract.MaintenanceEntry.COLUMN_PREV_MILEAGE + " DOUBLE, " +
                        MaintenanceContract.MaintenanceEntry.COLUMN_MILEAGE + " DOUBLE, " +
                        //MaintenanceContract.MaintenanceEntry.COLUMN_CURRENT_MPG + " DOUBLE, " +
                        MaintenanceContract.MaintenanceEntry.COLUMN_FUEL_ADDED + " DOUBLE)"

        private const val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + MaintenanceContract.MaintenanceEntry.TABLE_NAME
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SQL_CREATE_ENTRIES)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // Upgrade policy is to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES)
        onCreate(db)
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }

    @Throws(SQLiteConstraintException::class)
    fun addMaintenanceRecord(context: Context, maintenance: Maintenance): Boolean {
        // Get previous DB entries for vehicle
        val previousRecords: ArrayList<Maintenance> = getAllMaintenanceRecordsForVehicleId(maintenance.vehicleId)
        //val recordCount: Int = previousRecords.size

        // Gets the data repository in write mode
        val db = writableDatabase

        /*var averageMPG: Double = 0.0
        var totalMPG: Double = 0.0
        var mileageDifference: Double = 0.0
        var iterations: Int = 0;
        if (recordCount > 0 && maintenance.type == Maintenance.TYPE_GAS_FILL) {
            for (previousRecord: Maintenance in previousRecords) {
                //if (!previousRecord.equals(previousRecords.first())) {
                if (recordCount > 0 && previousRecord.type == Maintenance.TYPE_GAS_FILL) {
                    totalMPG += previousRecord.currentMPG
                //}
                    mileageDifference = maintenance.mileage - previousRecord.mileage
                iterations++
                }
            }

            val currentMPG = mileageDifference / maintenance.fuelAdded
            averageMPG = totalMPG / iterations
            maintenance.currentMPG = currentMPG
            Log.d(TAG, "Record: " + iterations + " | Avg MPG: " + averageMPG + " | Current MPG: " + currentMPG + " | Mileage diff: " + mileageDifference)
        } else if (recordCount > 0 && maintenance.type != Maintenance.TYPE_GAS_FILL) {
            maintenance.currentMPG = previousRecords.last().currentMPG
        } else {
            maintenance.currentMPG = 0.0
        }*/

        // Create a new map of values, where column names are the keys
        val values = ContentValues()
        values.put(MaintenanceContract.MaintenanceEntry.COLUMN_VEHICLE_ID, maintenance.vehicleId)
        values.put(MaintenanceContract.MaintenanceEntry.COLUMN_TYPE, maintenance.type)
        values.put(MaintenanceContract.MaintenanceEntry.COLUMN_DATE, maintenance.date)
        values.put(MaintenanceContract.MaintenanceEntry.COLUMN_PREV_MILEAGE, maintenance.previousMileage)
        values.put(MaintenanceContract.MaintenanceEntry.COLUMN_MILEAGE, maintenance.mileage)
        //values.put(MaintenanceContract.MaintenanceEntry.COLUMN_CURRENT_MPG, maintenance.currentMPG)
        values.put(MaintenanceContract.MaintenanceEntry.COLUMN_FUEL_ADDED, maintenance.fuelAdded)

        // Insert the new row, returning the primary key value of the new row
        val newRowId = db.insert(MaintenanceContract.MaintenanceEntry.TABLE_NAME, null, values)
        maintenance.id = newRowId.toInt()
        db.close()

        val vehicleDbHelper = VehicleDbHelper(context)
        val vehicle = vehicleDbHelper.getVehicle(maintenance.vehicleId)
        vehicle.mileage = maintenance.mileage
        vehicleDbHelper.updateVehicle(vehicle)

        return true
    }

    @Throws(SQLiteConstraintException::class)
    fun updateMaintenanceRecord(maintenance: Maintenance): Boolean {
        // Gets the data repository in write mode
        val db = writableDatabase

        // Create a new map of values, where column names are the keys
        val values = ContentValues()
        values.put(MaintenanceContract.MaintenanceEntry.COLUMN_VEHICLE_ID, maintenance.vehicleId)
        values.put(MaintenanceContract.MaintenanceEntry.COLUMN_TYPE, maintenance.type)
        values.put(MaintenanceContract.MaintenanceEntry.COLUMN_DATE, maintenance.date)
        values.put(MaintenanceContract.MaintenanceEntry.COLUMN_PREV_MILEAGE, maintenance.previousMileage)
        values.put(MaintenanceContract.MaintenanceEntry.COLUMN_MILEAGE, maintenance.mileage)
        //values.put(MaintenanceContract.MaintenanceEntry.COLUMN_CURRENT_MPG, maintenance.currentMPG)
        values.put(MaintenanceContract.MaintenanceEntry.COLUMN_FUEL_ADDED, maintenance.fuelAdded)

        // Update the row
        val where = "(" + MaintenanceContract.MaintenanceEntry.COLUMN_MAINTENANCE_ID + " = ?)"
        val selectionArgs: Array<String> = arrayOf(maintenance.id.toString())

        Log.d(TAG, "Updating maintenance ID: " + maintenance.id)

        db.update(MaintenanceContract.MaintenanceEntry.TABLE_NAME, values, where, selectionArgs)
        db.close()

        return true
    }

    @Throws(SQLiteConstraintException::class)
    fun deleteMaintenanceRecord(maintenanceId: Int): Boolean {
        // Gets the data repository in write mode
        val db = writableDatabase
        // Define 'where' part of query.
        val selection = MaintenanceContract.MaintenanceEntry.COLUMN_MAINTENANCE_ID + " LIKE ?"
        // Specify arguments in placeholder order.
        val selectionArgs = arrayOf(maintenanceId.toString())
        // Issue SQL statement.
        db.delete(MaintenanceContract.MaintenanceEntry.TABLE_NAME, selection, selectionArgs)
        db.close()

        return true
    }

    @Throws(SQLiteConstraintException::class)
    fun deleteAllMaintenanceRecordsForVehicleId(vehicleId: Int): Boolean {
        // Gets the data repository in write mode
        val db = writableDatabase
        // Define 'where' part of query.
        val selection = MaintenanceContract.MaintenanceEntry.COLUMN_VEHICLE_ID + " LIKE ?"
        // Specify arguments in placeholder order.
        val selectionArgs = arrayOf(vehicleId.toString())
        // Issue SQL statement.
        db.delete(MaintenanceContract.MaintenanceEntry.TABLE_NAME, selection, selectionArgs)
        db.close()

        return true
    }

    fun getMaintenanceRecord(maintenanceId: Int): Maintenance {
        var maintenance = Maintenance()
        val db = writableDatabase
        val cursor: Cursor?
        try {
            cursor = db.rawQuery("select * from " + MaintenanceContract.MaintenanceEntry.TABLE_NAME + " WHERE " + MaintenanceContract.MaintenanceEntry.COLUMN_MAINTENANCE_ID + "='" + maintenanceId + "'", null)
        } catch (e: SQLiteException) {
            // if table not yet present, create it
            db.execSQL(SQL_CREATE_ENTRIES)
            return Maintenance()
        }

        val id: Int
        val vehicleId: Int
        val type: String
        val date: String
        val previousMileage: Double
        val mileage: Double

        if (cursor!!.moveToFirst()) {
            id = cursor.getInt(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_MAINTENANCE_ID))
            vehicleId = cursor.getInt(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_VEHICLE_ID))
            type = cursor.getString(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_TYPE))
            date = cursor.getString(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_DATE))
            previousMileage = cursor.getDouble(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_PREV_MILEAGE))
            mileage = cursor.getDouble(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_MILEAGE))
            //val currentMPG = cursor.getDouble(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_CURRENT_MPG))
            val fuelAdded = cursor.getDouble(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_FUEL_ADDED))

            maintenance = Maintenance(id, vehicleId, type, date, previousMileage, mileage, fuelAdded)

            //Log.d(TAG, String.format(Locale.getDefault(), "DB got maintenance: ID: %d | Vehicle ID: %d | Type: %s | Mileage: %.1f | Current MPG: %.1f", maintenance.id, maintenance.vehicleId, maintenance.type, maintenance.mileage, maintenance.currentMPG))

        }
        cursor.close()
        db.close()

        return maintenance
    }

    fun getPreviousMaintenanceRecordForType(maintenanceId: Int, type: String): Maintenance {
        var maintenance = Maintenance()
        val db = writableDatabase
        val cursor: Cursor?
        try {
            cursor = db.rawQuery("select * from " + MaintenanceContract.MaintenanceEntry.TABLE_NAME + " WHERE " + MaintenanceContract.MaintenanceEntry.COLUMN_MAINTENANCE_ID + "<'" + maintenanceId + "' AND " + MaintenanceContract.MaintenanceEntry.COLUMN_TYPE + "='" + type + "' LIMIT 1", null)
        } catch (e: SQLiteException) {
            // if table not yet present, create it
            db.execSQL(SQL_CREATE_ENTRIES)
            return Maintenance()
        }

        val id: Int
        val vehicleId: Int
        val typeFound: String
        val date: String
        val mileage: Double
        val previousMileage: Double

        if (cursor!!.moveToFirst()) {
            id = cursor.getInt(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_MAINTENANCE_ID))
            vehicleId = cursor.getInt(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_VEHICLE_ID))
            typeFound = cursor.getString(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_TYPE))
            date = cursor.getString(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_DATE))
            previousMileage = cursor.getDouble(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_PREV_MILEAGE))
            mileage = cursor.getDouble(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_MILEAGE))
            //val currentMPG = cursor.getDouble(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_CURRENT_MPG))
            val fuelAdded = cursor.getDouble(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_FUEL_ADDED))

            maintenance = Maintenance(id, vehicleId, typeFound, date, previousMileage, mileage, fuelAdded)

            //Log.d(TAG, String.format(Locale.getDefault(), "DB got maintenance: ID: %d | Vehicle ID: %d | Type: %s | Mileage: %.1f | Current MPG: %.1f", maintenance.id, maintenance.vehicleId, maintenance.type, maintenance.mileage, maintenance.currentMPG))

        }
        cursor.close()
        db.close()

        return maintenance
    }


    fun getAllMaintenanceRecordsForVehicleId(vehicleId: Int): ArrayList<Maintenance> {
        val maintenanceList = ArrayList<Maintenance>()
        val db = writableDatabase
        val cursor: Cursor?
        try {
            cursor = db.rawQuery("select * from " + MaintenanceContract.MaintenanceEntry.TABLE_NAME + " WHERE " + MaintenanceContract.MaintenanceEntry.COLUMN_VEHICLE_ID + "='" + vehicleId + "'", null)
        } catch (e: SQLiteException) {
            db.execSQL(SQL_CREATE_ENTRIES)
            return ArrayList()
        }


        var id: Int
        var vehicleIdFromDb: Int
        var type: String
        var date: String
        var previousMileage: Double
        var mileage: Double

        if (cursor!!.moveToFirst()) {
            while (!cursor.isAfterLast) {
                id = cursor.getInt(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_MAINTENANCE_ID))
                vehicleIdFromDb = cursor.getInt(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_VEHICLE_ID))
                type = cursor.getString(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_TYPE))
                date = cursor.getString(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_DATE))
                previousMileage = cursor.getDouble(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_PREV_MILEAGE))
                mileage = cursor.getDouble(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_MILEAGE))
                //val currentMPG = cursor.getDouble(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_CURRENT_MPG))
                val fuelAdded = cursor.getDouble(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_FUEL_ADDED))


                maintenanceList.add(Maintenance(id, vehicleIdFromDb, type, date, previousMileage, mileage, fuelAdded))
                cursor.moveToNext()
            }
        }
        cursor.close()
        db.close()

        return maintenanceList
    }

    fun getAllMaintenanceRecordsForVehicleIdByType(vehicleId: Int, type: String): ArrayList<Maintenance> {
        val maintenanceList = ArrayList<Maintenance>()
        val db = writableDatabase
        val cursor: Cursor?
        try {
            cursor = db.rawQuery("select * from " + MaintenanceContract.MaintenanceEntry.TABLE_NAME + " WHERE " + MaintenanceContract.MaintenanceEntry.COLUMN_VEHICLE_ID + "='" + vehicleId + "'" + " AND " + MaintenanceContract.MaintenanceEntry.COLUMN_TYPE + "='" + type + "'", null)
        } catch (e: SQLiteException) {
            db.execSQL(SQL_CREATE_ENTRIES)
            return ArrayList()
        }


        var id: Int
        var vehicleIdFromDb: Int
        var typeFound: String
        var date: String
        var previousMileage: Double
        var mileage: Double

        if (cursor!!.moveToFirst()) {
            while (!cursor.isAfterLast) {
                id = cursor.getInt(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_MAINTENANCE_ID))
                vehicleIdFromDb = cursor.getInt(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_VEHICLE_ID))
                typeFound = cursor.getString(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_TYPE))
                date = cursor.getString(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_DATE))
                previousMileage = cursor.getDouble(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_PREV_MILEAGE))
                mileage = cursor.getDouble(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_MILEAGE))
                //val currentMPG = cursor.getDouble(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_CURRENT_MPG))
                val fuelAdded = cursor.getDouble(cursor.getColumnIndex(MaintenanceContract.MaintenanceEntry.COLUMN_FUEL_ADDED))


                maintenanceList.add(Maintenance(id, vehicleIdFromDb, typeFound, date, previousMileage, mileage, fuelAdded))
                cursor.moveToNext()
            }
        }
        cursor.close()
        db.close()

        return maintenanceList
    }

}