package com.infiniwaresolutions.mileageio.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.infiniwaresolutions.mileageio.models.Vehicle
import java.util.*


class VehicleDbHelper(context: Context?) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    companion object {
        // If you change the database schema, you must increment the database version.
        private const val TAG = "VehicleDbHelper"
        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "VehicleDatabase.db"

        private const val SQL_CREATE_ENTRIES =
                "CREATE TABLE IF NOT EXISTS " + VehicleContract.VehicleEntry.TABLE_NAME + " (" +
                        VehicleContract.VehicleEntry.COLUMN_VEHICLE_ID + " INTEGER PRIMARY KEY," +
                        VehicleContract.VehicleEntry.COLUMN_YEAR + " INTEGER, " +
                        VehicleContract.VehicleEntry.COLUMN_MAKE + " TEXT," +
                        VehicleContract.VehicleEntry.COLUMN_MODEL + " TEXT, " +
                        VehicleContract.VehicleEntry.COLUMN_MILEAGE + " DOUBLE, " +
                        VehicleContract.VehicleEntry.COLUMN_AVERAGE_MPG + " Double)"

        private const val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + VehicleContract.VehicleEntry.TABLE_NAME
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SQL_CREATE_ENTRIES)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // Upgrade policy is to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES)
        onCreate(db)
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }

    @Throws(SQLiteConstraintException::class)
    fun addVehicle(vehicle: Vehicle): Boolean {
        // Gets the data repository in write mode
        val db = writableDatabase

        // Create a new map of values, where column names are the keys
        val values = ContentValues()
        //values.put(VehicleContract.VehicleEntry.COLUMN_VEHICLE_ID, vehicle.id)
        values.put(VehicleContract.VehicleEntry.COLUMN_YEAR, vehicle.year)
        values.put(VehicleContract.VehicleEntry.COLUMN_MAKE, vehicle.make)
        values.put(VehicleContract.VehicleEntry.COLUMN_MODEL, vehicle.model)
        values.put(VehicleContract.VehicleEntry.COLUMN_MILEAGE, vehicle.mileage)
        values.put(VehicleContract.VehicleEntry.COLUMN_AVERAGE_MPG, vehicle.averageMPG)

        // Insert the new row, returning the primary key value of the new row
        val newRowId = db.insert(VehicleContract.VehicleEntry.TABLE_NAME, null, values)
        vehicle.id = newRowId.toInt()
        db.close()

        return true
    }

    @Throws(SQLiteConstraintException::class)
    fun updateVehicle(vehicle: Vehicle): Boolean {
        // Gets the data repository in write mode
        val db = writableDatabase

        // Create a new map of values, where column names are the keys
        val values = ContentValues()
        values.put(VehicleContract.VehicleEntry.COLUMN_YEAR, vehicle.year)
        values.put(VehicleContract.VehicleEntry.COLUMN_MAKE, vehicle.make)
        values.put(VehicleContract.VehicleEntry.COLUMN_MODEL, vehicle.model)
        values.put(VehicleContract.VehicleEntry.COLUMN_MILEAGE, vehicle.mileage)
        values.put(VehicleContract.VehicleEntry.COLUMN_AVERAGE_MPG, vehicle.averageMPG)

        // Update the row
        val where = "(" + VehicleContract.VehicleEntry.COLUMN_VEHICLE_ID + " = ?)"
        val selectionArgs: Array<String> = arrayOf(vehicle.id.toString())

        Log.d(TAG, "Updating vehicle ID: " + vehicle.id)

        db.update(VehicleContract.VehicleEntry.TABLE_NAME, values, where, selectionArgs)
        db.close()

        return true
    }

    @Throws(SQLiteConstraintException::class)
    fun deleteVehicle(vehicleId: Int): Boolean {
        // Gets the data repository in write mode
        val db = writableDatabase
        // Define 'where' part of query.
        val selection = VehicleContract.VehicleEntry.COLUMN_VEHICLE_ID + " LIKE ?"
        // Specify arguments in placeholder order.
        val selectionArgs = arrayOf(vehicleId.toString())
        // Issue SQL statement.
        db.delete(VehicleContract.VehicleEntry.TABLE_NAME, selection, selectionArgs)
        db.close()

        return true
    }

    fun getVehicle(vehicleId: Int): Vehicle {
        var vehicle = Vehicle()
        val db = writableDatabase
        val cursor: Cursor?
        try {
            cursor = db.rawQuery("select * from " + VehicleContract.VehicleEntry.TABLE_NAME + " WHERE " + VehicleContract.VehicleEntry.COLUMN_VEHICLE_ID + "='" + vehicleId + "'", null)
        } catch (e: SQLiteException) {
            // if table not yet present, create it
            db.execSQL(SQL_CREATE_ENTRIES)
            return Vehicle()
        }

        val id: Int
        val year: Int
        val make: String
        val model: String
        val mileage: Double
        val averageMPG: Double
        if (cursor!!.moveToFirst()) {
            id = cursor.getInt(cursor.getColumnIndex(VehicleContract.VehicleEntry.COLUMN_VEHICLE_ID))
            year = cursor.getInt(cursor.getColumnIndex(VehicleContract.VehicleEntry.COLUMN_YEAR))
            make = cursor.getString(cursor.getColumnIndex(VehicleContract.VehicleEntry.COLUMN_MAKE))
            model = cursor.getString(cursor.getColumnIndex(VehicleContract.VehicleEntry.COLUMN_MODEL))
            mileage = cursor.getDouble(cursor.getColumnIndex(VehicleContract.VehicleEntry.COLUMN_MILEAGE))
            averageMPG = cursor.getDouble(cursor.getColumnIndex(VehicleContract.VehicleEntry.COLUMN_AVERAGE_MPG))

            vehicle = Vehicle(id, year, make, model, mileage, averageMPG)

            Log.d(TAG, String.format(Locale.getDefault(), "DB got vehicle: ID: %d | Vehicle: %d %s %s | Mileage: %.1f | Average MPG: %.1f", vehicle.id, vehicle.year, vehicle.make, vehicle.model, vehicle.mileage, vehicle.averageMPG))
        }
        cursor.close()
        db.close()

        return vehicle
    }


    fun getAllVehicles(): ArrayList<Vehicle> {
        val vehicles = ArrayList<Vehicle>()
        val db = writableDatabase
        val cursor: Cursor?
        try {
            cursor = db.rawQuery("select * from " + VehicleContract.VehicleEntry.TABLE_NAME, null)
        } catch (e: SQLiteException) {
            db.execSQL(SQL_CREATE_ENTRIES)
            return ArrayList()
        }

        var vehicleId: Int
        var year: Int
        var make: String
        var model: String
        var mileage: Double
        var averageMPG: Double
        if (cursor!!.moveToFirst()) {
            while (!cursor.isAfterLast) {
                vehicleId = cursor.getInt(cursor.getColumnIndex(VehicleContract.VehicleEntry.COLUMN_VEHICLE_ID))
                year = cursor.getInt(cursor.getColumnIndex(VehicleContract.VehicleEntry.COLUMN_YEAR))
                make = cursor.getString(cursor.getColumnIndex(VehicleContract.VehicleEntry.COLUMN_MAKE))
                model = cursor.getString(cursor.getColumnIndex(VehicleContract.VehicleEntry.COLUMN_MODEL))
                mileage = cursor.getDouble(cursor.getColumnIndex(VehicleContract.VehicleEntry.COLUMN_MILEAGE))
                averageMPG = cursor.getDouble(cursor.getColumnIndex(VehicleContract.VehicleEntry.COLUMN_AVERAGE_MPG))

                vehicles.add(Vehicle(vehicleId, year, make, model, mileage, averageMPG))
                cursor.moveToNext()
            }
        }
        cursor.close()
        db.close()

        return vehicles
    }

}