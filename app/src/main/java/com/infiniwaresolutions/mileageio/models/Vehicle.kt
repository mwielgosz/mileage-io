package com.infiniwaresolutions.mileageio.models

import java.io.Serializable

class Vehicle : Serializable {
    var id: Int = 0
    var year: Int = 0
    var make: String? = null
    var model: String? = null
    var mileage: Double = 0.0
    var averageMPG: Double = 0.0

    constructor()

    constructor(year: Int, make: String, model: String, mileage: Double, averageMPG: Double) {
        this.year = year
        this.make = make
        this.model = model
        this.mileage = mileage
        this.averageMPG = averageMPG
    }

    constructor(id: Int, year: Int, make: String, model: String, mileage: Double, averageMPG: Double) {
        this.id = id
        this.year = year
        this.make = make
        this.model = model
        this.mileage = mileage
        this.averageMPG = averageMPG
    }

}
