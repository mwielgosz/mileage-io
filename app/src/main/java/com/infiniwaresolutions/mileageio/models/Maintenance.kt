package com.infiniwaresolutions.mileageio.models

import java.io.Serializable

class Maintenance : Serializable {
    companion object {
        const val TYPE_GAS_FILL = "type-gas-fill"
        const val TYPE_OIL_CHANGE = "type-oil-change"
        const val TYPE_OTHER = "type-other"
    }

    var id: Int = 0
    var vehicleId: Int = 0
    var type: String = ""
    var date: String = ""
    var previousMileage: Double = 0.0
    var mileage: Double = 0.0
    //var currentMPG: Double = 0.0
    var fuelAdded: Double = 0.0

    constructor()

    constructor(vehicleId: Int, type: String, date: String, previousMileage: Double, mileage: Double, fuelAdded: Double) {
        this.vehicleId = vehicleId
        this.type = type
        this.date = date
        this.previousMileage = previousMileage
        this.mileage = mileage
        this.fuelAdded = fuelAdded
    }

    /*constructor(vehicleId: Int, type: String, date: String, previousMileage: Double, mileage: Double, currentMPG: Double, fuelAdded: Double) {
        this.vehicleId = vehicleId
        this.type = type
        this.date = date
        this.previousMileage = previousMileage
        this.mileage = mileage
        this.currentMPG = currentMPG
        this.fuelAdded = fuelAdded
    }*/

    constructor(id: Int, vehicleId: Int, type: String, date: String, mileage: Double, fuelAdded: Double) {
        this.id = id
        this.vehicleId = vehicleId
        this.type = type
        this.date = date
        this.mileage = mileage
        this.fuelAdded = fuelAdded
    }

    constructor(id: Int, vehicleId: Int, type: String, date: String, previousMileage: Double, mileage: Double, fuelAdded: Double) {
        this.id = id
        this.vehicleId = vehicleId
        this.type = type
        this.date = date
        this.previousMileage = previousMileage
        this.mileage = mileage
        this.fuelAdded = fuelAdded
    }

}