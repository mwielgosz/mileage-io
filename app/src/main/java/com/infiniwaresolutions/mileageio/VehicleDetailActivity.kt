package com.infiniwaresolutions.mileageio

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.infiniwaresolutions.mileageio.models.Maintenance
import com.infiniwaresolutions.mileageio.models.Vehicle
import java.util.*

class VehicleDetailActivity : AppCompatActivity(), VehicleDetailFragment.OnVehicleDetailFragmentInteractionListener, MaintenanceListFragment.OnMaintenanceListFragmentInteractionListener {
    companion object {
        private const val TAG: String = "VehicleDetailActivity"
    }

    private lateinit var fabAddMaintenanceAction: FloatingActionButton

    override fun onVehicleDetailFragmentInteraction(vehicle: Vehicle) {
        Toast.makeText(this, String.format(Locale.getDefault(), "Selected for detail fragment: %d %s %s", vehicle.year, vehicle.make, vehicle.model), Toast.LENGTH_SHORT).show()
    }

    override fun onVehicleDetailFragmentBack() {
        Log.d(TAG, "Vehicle detail fragment ended")

        this.finish()
    }

    override fun onMaintenanceListFragmentInteraction(maintenance: Maintenance) {
        Log.d(TAG, String.format(Locale.getDefault(), "Clicked maintenance ID: %d | VehicleID : %d | Type: %s | Date: %s | Mileage: %.1f", maintenance.id, maintenance.vehicleId, maintenance.type, maintenance.date, maintenance.mileage))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vehicle_detail)

        fabAddMaintenanceAction = findViewById(R.id.fab_add_maintenance_action)
        fabAddMaintenanceAction.setOnClickListener {
            Log.d(TAG, "Add maintenance button pressed")
            //TODO: Add onAddEditMaintenance function
        }

        val vehicle = intent.extras.getSerializable(MainActivity.EXTRA_VEHICLE) as Vehicle

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.vehicle_detail_title)

        supportFragmentManager.beginTransaction()
                .add(R.id.root_container_vehicle_detail, VehicleDetailFragment.newInstance(vehicle.id), "vehicleDetailFragment")
                .addToBackStack(null)
                .commit()
    }

    override fun onBackPressed() {
        Log.d(TAG, "onBack. backStackEntryCount: " + supportFragmentManager.backStackEntryCount)
        if (supportFragmentManager.backStackEntryCount >= 2) {
            supportFragmentManager.popBackStack()
            supportActionBar?.title = getString(R.string.vehicle_detail_title)
        } else if (supportFragmentManager.backStackEntryCount > 1) {
            supportFragmentManager.popBackStack()
        } else {
            this.finish();
        }
    }

}
