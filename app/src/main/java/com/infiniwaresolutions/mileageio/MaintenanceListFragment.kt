package com.infiniwaresolutions.mileageio

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.DividerItemDecoration.VERTICAL
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.infiniwaresolutions.mileageio.database.MaintenanceDbHelper
import com.infiniwaresolutions.mileageio.models.Maintenance
import com.infiniwaresolutions.mileageio.models.Vehicle


/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [MaintenanceListFragment.OnMaintenanceListFragmentInteractionListener] interface.
 */
class MaintenanceListFragment : Fragment() {
    companion object {
        private const val TAG = "MaintenanceListFragment"
        private const val ARG_VEHICLE = "vehicle"

        @JvmStatic
        fun newInstance(vehicle: Vehicle) =
                MaintenanceListFragment().apply {
                    Log.d(TAG, "Creating MaintenanceListFragment")
                    arguments = Bundle().apply {
                        putSerializable(ARG_VEHICLE, vehicle)
                    }
                }
    }

    private var listener: OnMaintenanceListFragmentInteractionListener? = null

    private lateinit var maintenanceDB: MaintenanceDbHelper
    private var vehicle: Vehicle = Vehicle()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)

        arguments?.let {
            vehicle = it.getSerializable(ARG_VEHICLE) as Vehicle
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_maintenance_list, container, false)
        val recyclerView = view.findViewById(R.id.rv_maintenance) as RecyclerView

        Log.d(TAG, "Creating MaintenanceListFragment for vehicle ID: " + vehicle.id)
        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView.adapter = MaintenanceAdapter(vehicle, maintenanceDB.getAllMaintenanceRecordsForVehicleId(vehicle.id)) { maintenanceItem: Maintenance ->
            maintenanceItemClicked(maintenanceItem)
        }
        recyclerView.setHasFixedSize(true)
        val itemDecor = DividerItemDecoration(context, VERTICAL)
        recyclerView.addItemDecoration(itemDecor)

        return view
    }

    private fun maintenanceItemClicked(maintenance: Maintenance) {
        listener?.onMaintenanceListFragmentInteraction(maintenance)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnMaintenanceListFragmentInteractionListener) {
            listener = context
            maintenanceDB = MaintenanceDbHelper(context)
        } else {
            throw RuntimeException(context.toString() + " must implement OnMaintenanceListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnMaintenanceListFragmentInteractionListener {
        fun onMaintenanceListFragmentInteraction(maintenance: Maintenance)
    }

}
