package com.infiniwaresolutions.mileageio

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import com.infiniwaresolutions.mileageio.database.VehicleDbHelper
import com.infiniwaresolutions.mileageio.models.Vehicle

class AddEditVehicleActivity : AppCompatActivity() {

    companion object {
        private const val TAG: String = "AddEditVehicleActivity"
    }

    private var editMode: Boolean = false
    private var vehicle: Vehicle = Vehicle()

    private lateinit var etYear: EditText
    private lateinit var etMake: EditText
    private lateinit var etModel: EditText
    private lateinit var etMileage: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_edit_vehicle)

        etYear = findViewById(R.id.et_add_vehicle_year) as EditText
        etMake = findViewById(R.id.et_add_vehicle_make) as EditText
        etModel = findViewById(R.id.et_add_vehicle_model) as EditText
        etMileage = findViewById(R.id.et_add_vehicle_mileage) as EditText

        editMode = intent.extras.getBoolean(MainActivity.EXTRA_EDIT_MODE)
        vehicle = intent.extras.getSerializable(MainActivity.EXTRA_VEHICLE) as Vehicle

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (editMode) {
            supportActionBar?.title = getString(R.string.edit_vehicle_title)

            // Fill in values in Edit Mode
            Log.d(TAG, "Edit: " + editMode + " | Vehicle ID: " + vehicle.id)
            etYear.setText(vehicle.year.toString())
            etMake.setText(vehicle.make)
            etModel.setText(vehicle.model)
            etMileage.setText(vehicle.mileage.toString())
        } else {
            supportActionBar?.title = getString(R.string.add_vehicle_title)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.add_edit_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> onCancel()
            R.id.save_vehicle_item -> onSaveVehicle(this.vehicle)
        }
        return true
    }

    private fun onSaveVehicle(vehicle: Vehicle?) {
        // Error detection
        if (etYear.text.isEmpty() || etYear.text.toString().toIntOrNull() == null) {
            Toast.makeText(this, "Vehicle year missing or contains text", Toast.LENGTH_SHORT).show()
            return
        }
        if (etMake.text.isEmpty()) {
            Toast.makeText(this, "Vehicle make missing", Toast.LENGTH_SHORT).show()
            return
        }
        if (etModel.text.isEmpty()) {
            Toast.makeText(this, "Vehicle model missing", Toast.LENGTH_SHORT).show()
            return
        }
        if (etMileage.text.isEmpty() || etYear.text.toString().toDoubleOrNull() == null) {
            Toast.makeText(this, "Vehicle mileage missing or contains text", Toast.LENGTH_SHORT).show()
            return
        }

        if (vehicle != null) {
            vehicle.year = etYear.text.toString().toInt()
            vehicle.make = etMake.text.toString()
            vehicle.model = etModel.text.toString()
            vehicle.mileage = etMileage.text.toString().toDouble()


            val vehicleDb = VehicleDbHelper(this)
            if (editMode) {
                vehicleDb.updateVehicle(vehicle)
            } else {
                vehicleDb.addVehicle(vehicle)
            }

            intent.putExtra(MainActivity.EXTRA_VEHICLE, vehicle)
            setResult(Activity.RESULT_OK, intent)
        }

        this.finish()
    }

    private fun onCancel() {
        setResult(Activity.RESULT_CANCELED)
        this.finish()
    }

}
