package com.infiniwaresolutions.mileageio


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.infiniwaresolutions.mileageio.database.MaintenanceDbHelper
import com.infiniwaresolutions.mileageio.models.Maintenance
import com.infiniwaresolutions.mileageio.models.Vehicle
import kotlinx.android.synthetic.main.list_maintenance_item.view.*
import java.text.SimpleDateFormat
import java.util.*

class MaintenanceAdapter(private val vehicle: Vehicle, private val maintenanceData: ArrayList<Maintenance>, private val clickListener: (Maintenance) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        private const val TAG: String = "TAG"
    }

    class MaintenanceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(maintenance: Maintenance, clickListener: (Maintenance) -> Unit) {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val maintenanceDate = simpleDateFormat.format(Date.parse(maintenance.date))

            itemView.tv_maintenance_type.text = String.format(Locale.getDefault(), "%s", maintenance.type)
            itemView.tv_maintenance_date.text = maintenanceDate
            itemView.tv_maintenance_mileage.text = String.format(Locale.getDefault(), "%.1f".format(maintenance.mileage))

            // TODO: Replace with static method for calculating MPG
            var mpg = 0.0
            if (maintenance.type == Maintenance.TYPE_GAS_FILL) {
                val previousRecords = MaintenanceDbHelper(itemView.context).getAllMaintenanceRecordsForVehicleIdByType(maintenance.vehicleId, maintenance.type)

                for (previousRecord in previousRecords) {
                    if (previousRecord.id == maintenance.id) {
                        break
                    } else if (previousRecord.id < maintenance.id) {
                        val mileageDifference: Double = maintenance.mileage - previousRecord.mileage

                        if (mileageDifference != 0.0) {
                            mpg = mileageDifference / maintenance.fuelAdded
                        }
                    }
                }
                if (!mpg.isNaN()) {
                    itemView.tv_maintenance_mpg.text = String.format(Locale.getDefault(), "Current MPG: %.2f", mpg)
                } else {
                    itemView.tv_maintenance_mpg.text = String.format(Locale.getDefault(), "Current MPG: N/A")
                }
            } else {
                itemView.tv_maintenance_mpg.text = String.format(Locale.getDefault(), "Current MPG: N/A")
            }

            itemView.setOnClickListener {
                clickListener(maintenance)
            }
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        // create a new view
        val maintenanceView = LayoutInflater.from(parent.context).inflate(R.layout.list_maintenance_item, parent, false) as RelativeLayout
        // set the view's size, margins, paddings and layout parameters
        return MaintenanceViewHolder(maintenanceView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MaintenanceViewHolder).bind(maintenanceData[position], clickListener)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = maintenanceData.size
}
