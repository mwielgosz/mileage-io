package com.infiniwaresolutions.mileageio

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.infiniwaresolutions.mileageio.models.Vehicle
import kotlinx.android.synthetic.main.list_vehicle_item.view.*
import java.util.*

class VehicleAdapter(private val vehicleData: ArrayList<Vehicle>, private val clickListener: (Vehicle) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        private const val TAG: String = "TAG"
    }

    class VehicleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(vehicle: Vehicle, clickListener: (Vehicle) -> Unit) {
            itemView.tv_vehicle_name.text = String.format(Locale.getDefault(), "%d %s %s", vehicle.year, vehicle.make, vehicle.model)
            itemView.tv_vehicle_mileage.text = String.format(Locale.getDefault(), "%.1f".format(vehicle.mileage))
            if (vehicle.averageMPG != 0.0 && !vehicle.averageMPG.isNaN() && !vehicle.averageMPG.isInfinite()) {
            itemView.tv_vehicle_mpg.text = String.format(Locale.getDefault(), "Average MPG: %.2f", vehicle.averageMPG)
            } else {
                itemView.tv_vehicle_mpg.text = String.format(Locale.getDefault(), "Average MPG: N/A")
            }
            itemView.setOnClickListener {
                clickListener(vehicle)
            }
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        // create a new view
        val cardView = LayoutInflater.from(parent.context).inflate(R.layout.list_vehicle_item, parent, false) as CardView
        // set the view's size, margins, paddings and layout parameters
        return VehicleViewHolder(cardView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as VehicleViewHolder).bind(vehicleData[position], clickListener)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = vehicleData.size
}
