package com.infiniwaresolutions.mileageio

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import com.infiniwaresolutions.mileageio.database.MaintenanceDbHelper
import com.infiniwaresolutions.mileageio.database.VehicleDbHelper
import com.infiniwaresolutions.mileageio.models.Maintenance
import com.infiniwaresolutions.mileageio.models.Vehicle
import java.util.*


class MainActivity : AppCompatActivity() {
    companion object {
        private const val TAG: String = "MainActivity"
        const val EXTRA_VEHICLE: String = "extra-vehicle"
        const val EXTRA_EDIT_MODE: String = "extra-edit-mode"
        const val REQUEST_CODE_VEHICLE_DETAIL: Int = 1
        const val REQUEST_CODE_ADD_EDIT_VEHICLE: Int = 2
    }

    private lateinit var vehicleDB: VehicleDbHelper
    private lateinit var vehicleList: ArrayList<Vehicle>
    private lateinit var maintenanceDB: MaintenanceDbHelper
    private lateinit var rvVehicleList: RecyclerView
    private lateinit var fabAddVehicleAction: FloatingActionButton


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        vehicleDB = VehicleDbHelper(this)
        vehicleList = vehicleDB.getAllVehicles()

        rvVehicleList = findViewById(R.id.rv_vehicles)
        rvVehicleList.setHasFixedSize(true)
        rvVehicleList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        fabAddVehicleAction = findViewById(R.id.fab_add_vehicle_action)
        fabAddVehicleAction.setOnClickListener {
            Log.d(TAG, "Add vehicle button pressed")
            val intent = Intent(this, AddEditVehicleActivity::class.java)
            intent.putExtra(EXTRA_VEHICLE, Vehicle())
            intent.putExtra(EXTRA_EDIT_MODE, false)
            startActivityForResult(intent, REQUEST_CODE_ADD_EDIT_VEHICLE)
            //onAddEditVehicleFragmentInteraction(false, Vehicle())
        }

        if (savedInstanceState == null) {
            // Fill test database: FOR TESTING ONLY
            fillTestDatabase() //TODO: REMOVE WHEN NOT TESTING
        }
    }

    override fun onBackPressed() {
        Log.d(TAG, "onBack. backStackEntryCount: " + supportFragmentManager.backStackEntryCount)
        if (supportFragmentManager.backStackEntryCount == 1) {
            supportFragmentManager.popBackStack()
            supportActionBar?.title = getString(R.string.app_name)
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
            fabAddVehicleAction.show()
            initVehicleDB()
        } else if (supportFragmentManager.backStackEntryCount > 1) {
            supportFragmentManager.popBackStack()
        } else {
            this.finish();
        }
    }

    override fun onResume() {
        super.onResume()
        initVehicleDB()
    }

    private fun initVehicleDB() {
        Log.d(TAG, "Vehicle DB init")
        vehicleDB = VehicleDbHelper(this)
        vehicleList = vehicleDB.getAllVehicles()

        // TODO: Replace with static method for calculating MPG
        for (vehicle in vehicleList) {
            var miles = 0.0
            var fuelAdded = 0.0
            val maintenanceRecords = MaintenanceDbHelper(this).getAllMaintenanceRecordsForVehicleIdByType(vehicle.id, Maintenance.TYPE_GAS_FILL)
            for (maintenance in maintenanceRecords) {
                val difference = maintenance.mileage - maintenance.previousMileage
                miles += difference
                //vehicle.mileage += difference
                Log.d(TAG, "Updated miles: " + miles + " | difference: " + difference)
                fuelAdded += maintenance.fuelAdded
            }

            vehicle.averageMPG = miles / fuelAdded
            //vehicle.mileage += miles
            Log.d(TAG, "Init miles: " + miles + " | fuelAdded: " + fuelAdded + " | mpg: " + vehicle.averageMPG)
            vehicleDB.updateVehicle(vehicle)
        }

        rvVehicleList.adapter = VehicleAdapter(vehicleList) { vehicleItem: Vehicle ->
            Log.d(TAG, String.format(Locale.getDefault(), "Clicked vehicle ID: %d | Vehicle: %d %s %s | Mileage: %.1f", vehicleItem.id, vehicleItem.year, vehicleItem.make, vehicleItem.model, vehicleItem.mileage))

            val intent = Intent(this, VehicleDetailActivity::class.java)
            intent.putExtra(EXTRA_VEHICLE, vehicleItem)
            startActivityForResult(intent, REQUEST_CODE_VEHICLE_DETAIL)
        }
        rvVehicleList.setHasFixedSize(true)
        rvVehicleList.visibility = View.VISIBLE
    }

    private fun fillTestDatabase() {
        maintenanceDB = MaintenanceDbHelper(this)

        // Test object data for vehicles
        if (vehicleDB.getAllVehicles().isEmpty()) {
            Log.d(TAG, "Filling test vehicle database")

            val currentDate = Date().toString()

            var vehicle = Vehicle(0, 1999, "Saab", "9-5", 55300.0, 0.0)
            vehicleDB.addVehicle(vehicle)
            // Test object data for maintenance
            if (maintenanceDB.getAllMaintenanceRecordsForVehicleId(vehicle.id).isEmpty()) {
                Log.d(TAG, "Filling test maintenance database. Vehicle ID: " + vehicle.id)
                var prevMileage = vehicle.mileage
                var mileage = vehicle.mileage + 200
                var maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, prevMileage, mileage, 10.0)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                prevMileage = mileage
                mileage += 350
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, prevMileage, mileage, 10.0)
                prevMileage = mileage
                mileage += 200
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, prevMileage, mileage, 10.0)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_OIL_CHANGE, currentDate, prevMileage, mileage, 0.0)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                prevMileage = mileage
                mileage += 200
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, prevMileage, mileage, 10.0)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_OTHER, currentDate, prevMileage, mileage, 0.0)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                prevMileage = mileage
                mileage += 350
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, prevMileage, mileage, 10.0)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
            } else {
                Log.d(TAG, "Database has maintenance records for vehicle ID: " + vehicle.id)
            }
            vehicle = Vehicle(1, 2000, "Dodge", "Durango", 154000.5, 0.0)
            vehicleDB.addVehicle(vehicle)
            // Test object data for maintenance
            if (maintenanceDB.getAllMaintenanceRecordsForVehicleId(vehicle.id).isEmpty()) {
                var prevMileage = vehicle.mileage
                var mileage = vehicle.mileage
                Log.d(TAG, "Filling test maintenance database. Vehicle ID: " + vehicle.id)
                var maintenance = Maintenance(vehicle.id, Maintenance.TYPE_OTHER, currentDate, prevMileage, mileage, 0.0)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_OTHER, currentDate, prevMileage, mileage, 0.0)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_OIL_CHANGE, currentDate, prevMileage, mileage, 0.0)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_OTHER, currentDate, prevMileage, mileage, 0.0)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
            } else {
                Log.d(TAG, "Database has maintenance records for vehicle ID: " + vehicle.id)
            }
            vehicle = Vehicle(2, 1985, "Ford", "F-150", 86900.0, 0.0)
            vehicleDB.addVehicle(vehicle)
            // Test object data for maintenance
            if (maintenanceDB.getAllMaintenanceRecordsForVehicleId(vehicle.id).isEmpty()) {
                var prevMileage = vehicle.mileage
                var mileage = vehicle.mileage + 100
                Log.d(TAG, "Filling test maintenance database. Vehicle ID: " + vehicle.id)
                var maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, prevMileage, mileage, 10.0)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                prevMileage = mileage
                mileage += 200
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, prevMileage, mileage, 10.0)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                prevMileage = mileage
                mileage += 400
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, prevMileage, mileage, 10.0)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                prevMileage = mileage
                mileage += 350
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, prevMileage, mileage, 10.0)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
            } else {
                Log.d(TAG, "Database has maintenance records for vehicle ID: " + vehicle.id)
            }
            /*vehicle = Vehicle(3, 2018, "Ford", "Mustang", 153.4, 0.0)
            vehicleDB.addVehicle(vehicle)
            // Test object data for maintenance
            if (maintenanceDB.getAllMaintenanceRecordsForVehicleId(vehicle.id).isEmpty()) {
                Log.d(TAG, "Filling test maintenance database. Vehicle ID: " + vehicle.id)
                var maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 200000.0, 20.34)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 15.4, 11.23)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 16.3, 24.34)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 12.1, 5.3)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
            } else {
                Log.d(TAG, "Database has maintenance records for vehicle ID: " + vehicle.id)
            }
            vehicle = Vehicle(4, 1966, "Dodge", "Charger", 63003.2, 0.0)
            vehicleDB.addVehicle(vehicle)
            // Test object data for maintenance
            if (maintenanceDB.getAllMaintenanceRecordsForVehicleId(vehicle.id).isEmpty()) {
                Log.d(TAG, "Filling test maintenance database. Vehicle ID: " + vehicle.id)
                var maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 200000.0, 20.34)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 15.4, 11.23)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 16.3, 24.34)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 12.1, 5.3)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
            } else {
                Log.d(TAG, "Database has maintenance records for vehicle ID: " + vehicle.id)
            }
            vehicle = Vehicle(5, 1920, "Ford", "Model T", 3566.3, 0.0)
            vehicleDB.addVehicle(vehicle)
            // Test object data for maintenance
            if (maintenanceDB.getAllMaintenanceRecordsForVehicleId(vehicle.id).isEmpty()) {
                Log.d(TAG, "Filling test maintenance database. Vehicle ID: " + vehicle.id)
                var maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 200000.0, 20.34)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 15.4, 11.23)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 16.3, 24.34)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 12.1, 5.3)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
            } else {
                Log.d(TAG, "Database has maintenance records for vehicle ID: " + vehicle.id)
            }
            vehicle = Vehicle(6, 2000, "Tesla", "Turbo", 250038.5, 0.0)
            vehicleDB.addVehicle(vehicle)
            // Test object data for maintenance
            if (maintenanceDB.getAllMaintenanceRecordsForVehicleId(vehicle.id).isEmpty()) {
                Log.d(TAG, "Filling test maintenance database. Vehicle ID: " + vehicle.id)
                var maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 200000.0, 20.34)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 15.4, 11.23)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 16.3, 24.34)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 12.1, 5.3)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
            } else {
                Log.d(TAG, "Database has maintenance records for vehicle ID: " + vehicle.id)
            }
            vehicle = Vehicle(7, 2005, "Toyota", "Corolla", 95450.5, 0.0)
            vehicleDB.addVehicle(vehicle)
            // Test object data for maintenance
            if (maintenanceDB.getAllMaintenanceRecordsForVehicleId(vehicle.id).isEmpty()) {
                Log.d(TAG, "Filling test maintenance database. Vehicle ID: " + vehicle.id)
                var maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 200000.0, 20.34)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 15.4, 11.23)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 16.3, 24.34)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 12.1, 5.3)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
            } else {
                Log.d(TAG, "Database has maintenance records for vehicle ID: " + vehicle.id)
            }
            vehicle = Vehicle(8, 2006, "Honda", "Civic", 54890.3, 0.0)
            vehicleDB.addVehicle(vehicle)
            // Test object data for maintenance
            if (maintenanceDB.getAllMaintenanceRecordsForVehicleId(vehicle.id).isEmpty()) {
                Log.d(TAG, "Filling test maintenance database. Vehicle ID: " + vehicle.id)
                var maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 200000.0, 20.34)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 15.4, 11.23)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 16.3, 24.34)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
                maintenance = Maintenance(vehicle.id, Maintenance.TYPE_GAS_FILL, currentDate, 12.1, 5.3)
                maintenanceDB.addMaintenanceRecord(this, maintenance)
            } else {
                Log.d(TAG, "Database has maintenance records for vehicle ID: " + vehicle.id)
            }*/
        } else {
            Log.d(TAG, "Database has vehicles")
        }
    }

}
