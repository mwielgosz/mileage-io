package com.infiniwaresolutions.mileageio

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.util.Log
import android.view.*
import android.widget.LinearLayout
import android.widget.Toast
import com.infiniwaresolutions.mileageio.MainActivity.Companion.REQUEST_CODE_ADD_EDIT_VEHICLE
import com.infiniwaresolutions.mileageio.database.MaintenanceDbHelper
import com.infiniwaresolutions.mileageio.database.VehicleDbHelper
import com.infiniwaresolutions.mileageio.models.Vehicle
import kotlinx.android.synthetic.main.list_vehicle_item.view.*
import java.util.*

/**
 * A fragment representing Vehicle Detail CardView and a list of Maintenance records.
 * Activities containing this fragment MUST implement the
 * [VehicleDetailFragment.OnVehicleDetailFragmentInteractionListener] interface.
 */
class VehicleDetailFragment : Fragment() {
    companion object {
        private const val TAG = "VehicleDetailFragment"
        private const val ARG_VEHICLE_ID = "vehicle-id"

        @JvmStatic
        fun newInstance(vehicleId: Int) =
                VehicleDetailFragment().apply {
                    Log.d(TAG, "Creating VehicleDetailFragment. Vehicle ID: $vehicleId")
                    arguments = Bundle().apply {
                        putInt(ARG_VEHICLE_ID, vehicleId)
                    }
                }
    }

    private var listener: OnVehicleDetailFragmentInteractionListener? = null

    private lateinit var vehicleCardViewContainer: LinearLayout
    private lateinit var vehicleCardView: CardView
    private lateinit var vehicleDB: VehicleDbHelper
    private lateinit var vehicle: Vehicle
    private var vehicleId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)

        vehicleDB = VehicleDbHelper(context)

        arguments?.let {
            vehicleId = it.getInt(ARG_VEHICLE_ID)
            vehicle = vehicleDB.getVehicle(vehicleId)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_vehicle_detail_list, container, false)
        vehicleCardViewContainer = view.findViewById(R.id.vehicle_card_container)
        vehicleCardView = LayoutInflater.from(context).inflate(R.layout.list_vehicle_item, container, false) as CardView

        vehicleCardView.tv_vehicle_name.text = String.format(Locale.getDefault(), "%d %s %s", vehicle.year, vehicle.make, vehicle.model)
        vehicleCardView.tv_vehicle_mileage.text = String.format(Locale.getDefault(), "%.1f".format(vehicle.mileage))
        if (vehicle.averageMPG != 0.0 && !vehicle.averageMPG.isNaN() && !vehicle.averageMPG.isInfinite()) {
            vehicleCardView.tv_vehicle_mpg.text = String.format(Locale.getDefault(), "Average MPG: %.2f", vehicle.averageMPG)
        } else {
            vehicleCardView.tv_vehicle_mpg.text = String.format(Locale.getDefault(), "Average MPG: N/A")
        }

        vehicleCardViewContainer.addView(vehicleCardView)

        activity?.supportFragmentManager
                ?.beginTransaction()
                ?.add(R.id.maintenance_container, MaintenanceListFragment.newInstance(vehicle), "maintenanceListFragment")
                ?.commit()

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnVehicleDetailFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnVehicleDetailFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.vehicle_detail_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> listener?.onVehicleDetailFragmentBack()
            R.id.edit_vehicle_item -> editVehicle()
            R.id.delete_vehicle_item -> deleteVehicle()
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == MainActivity.REQUEST_CODE_ADD_EDIT_VEHICLE) {
                if (data != null) {
                    vehicle = data.getSerializableExtra(MainActivity.EXTRA_VEHICLE) as Vehicle
                    vehicleCardView.tv_vehicle_name.text = String.format(Locale.getDefault(), "%d %s %s", vehicle.year, vehicle.make, vehicle.model)
                    vehicleCardView.tv_vehicle_mileage.text = String.format(Locale.getDefault(), "%.1f".format(vehicle.mileage))
                }
            }
        }
    }

    private fun editVehicle() {
        Log.d(TAG, "Edit vehicle button pressed")
        val intent = Intent(context, AddEditVehicleActivity::class.java)
        intent.putExtra(MainActivity.EXTRA_VEHICLE, vehicle)
        intent.putExtra(MainActivity.EXTRA_EDIT_MODE, true)
        startActivityForResult(intent, REQUEST_CODE_ADD_EDIT_VEHICLE)
    }

    private fun deleteVehicle() {
        // Setup AlertDialog to prompt user for deletion of vehicle & records
        val builder = AlertDialog.Builder(context)
        builder.setTitle(getString(R.string.alert_delete_vehicle_title))
        builder.setMessage(getString(R.string.alert_delete_vehicle_message))

        builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
            vehicleDB.deleteVehicle(vehicleId)
            MaintenanceDbHelper(context).deleteAllMaintenanceRecordsForVehicleId(vehicleId)
            dialog.dismiss()
            listener?.onVehicleDetailFragmentBack()
            Toast.makeText(context, getString(R.string.toast_delete_vehicle), Toast.LENGTH_SHORT).show()
        }

        builder.setNegativeButton(android.R.string.no) { _, _ -> }

        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    interface OnVehicleDetailFragmentInteractionListener {
        fun onVehicleDetailFragmentInteraction(vehicle: Vehicle)
        fun onVehicleDetailFragmentBack()
    }

}
